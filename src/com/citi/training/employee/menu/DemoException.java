package com.citi.training.employee.menu;

@SuppressWarnings("serial")
public class DemoException extends RuntimeException {				//12.24am on 8/14/19
	
   public DemoException(String message) {
	   super(message);
   }
}
